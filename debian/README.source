Implementation:

 * Package contains source of another "guest" package. Host package
   named "guest-pkg".

 * Package contains "guest" package sources, but not upstream sources.
   Upstream sources will be fetched using "uscan". After download
   orig.tar integrity is checked with known SHA256 digest.

 * Host package version (without debian revision) should match version
   of guest package with debian revision.

 * debian/watch file of a host package meant to be the same as
   debian/watch of guest package for notifications about new upstream
   versions.

 * Installer is implemented as shell script optionally installed as APT
   post-invoke handler.

 * Before attempting to build and install guest package(s) APT
   post-invoke handler check for broken packages using `apt-get check`
   and abort on any problem.

 * When "guest" package is built, its debian revision number is
   changed by appending "~local" to clearly mark generated packages.

 * During build of "guest" packages "host" package is injected to
   Guest's Depends to make sure generated packages will be removed on
   "host" package remove/purge.

 * Some capabilities(7) are dropped before invocation of
   `dpkg-buildpackage`.

 * Host package uses debconf to confirm downloading and building with
   user.

 * If user choose to remove "guest" package automatic re-installation of
   "guest" package(s) will be avoided.

 * Upgrades are gracefully handled i.e. if host package have bundled
   guest package with version higher than installed then it will be
   built into package and upgraded (semi-)automatically, if prior user
   decision allows it.

 * To rebuild guest package on host package upgrade it is enough to
   bump debian revision number of a guest package. Increment of debian
   revision of host package will not rebuild guest.
   This will work only if user confirmed installation of post-invoke
   handler.

 * `dpkg-reconfigure guest-pkg` may be used to build and install guest
   packages. Otherwise guest package(s) may be built and installed
   from APT post-invoke handler.

 * Because DPKG database is still locked when host package is installed
   the guest package can't be immediately built yet. On host package
   upgrade guest package will be automatically built and upgraded from
   APT post-invoke handler that is not yet available on first install
   of a host package. Currently there is no known way to automatically
   install guest package at the same time with its host. On first
   install of a host package bullding and installation of guest package
   will be delayed till next apt-get operation.

 * Generated guest packages will be saved to "/usr/src/host-pkg" (but
   deleted on purge of a host package).
